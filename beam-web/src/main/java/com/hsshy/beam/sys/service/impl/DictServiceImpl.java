package com.hsshy.beam.sys.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.hsshy.beam.sys.dao.DictMapper;
import com.hsshy.beam.sys.entity.Dict;
import com.hsshy.beam.sys.service.IDictService;
import org.springframework.stereotype.Service;

/**
 * 字典表
 *
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-10-10 21:39:07
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictMapper, Dict> implements IDictService {


}
