package com.hsshy.beam.sys.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hsshy.beam.sys.entity.Dict;

/**
 * 字典表
 * 
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-10-10 21:39:07
 */
public interface DictMapper extends BaseMapper<Dict> {
	
}
