package com.hsshy.beam.sys.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hsshy.beam.common.base.service.ICommonService;
import com.hsshy.beam.sys.entity.Dict;

/**
 * 字典表
 *
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-10-10 21:39:07
 */
public interface IDictService extends IService<Dict> {



}
