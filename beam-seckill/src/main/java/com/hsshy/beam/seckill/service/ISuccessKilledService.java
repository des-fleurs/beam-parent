package com.hsshy.beam.seckill.service;
import com.baomidou.mybatisplus.extension.service.IService;
import com.hsshy.beam.seckill.entity.SuccessKilled;
/**
 * 秒杀成功明细表
 *
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-11-20 14:52:22
 */
public interface ISuccessKilledService extends IService<SuccessKilled> {



}
