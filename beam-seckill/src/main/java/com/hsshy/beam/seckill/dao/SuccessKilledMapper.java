package com.hsshy.beam.seckill.dao;
import com.hsshy.beam.seckill.entity.SuccessKilled;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
/**
 * 秒杀成功明细表
 * 
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-11-20 14:52:22
 */
public interface SuccessKilledMapper extends BaseMapper<SuccessKilled> {
	
}
