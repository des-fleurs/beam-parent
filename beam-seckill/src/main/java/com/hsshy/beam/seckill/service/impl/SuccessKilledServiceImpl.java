package com.hsshy.beam.seckill.service.impl;
import com.hsshy.beam.seckill.dao.SuccessKilledMapper;
import com.hsshy.beam.seckill.entity.SuccessKilled;
import com.hsshy.beam.seckill.service.ISuccessKilledService;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * 秒杀成功明细表
 *
 * @author hs
 * @email 457030599@qq.com
 * @date 2018-11-20 14:52:22
 */
@Service
public class SuccessKilledServiceImpl extends ServiceImpl<SuccessKilledMapper, SuccessKilled> implements ISuccessKilledService {


}
