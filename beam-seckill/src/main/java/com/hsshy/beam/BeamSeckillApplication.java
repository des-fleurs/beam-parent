package com.hsshy.beam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeamSeckillApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeamSeckillApplication.class, args);
    }

}
