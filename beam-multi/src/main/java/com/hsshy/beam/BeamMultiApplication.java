package com.hsshy.beam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeamMultiApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeamMultiApplication.class, args);
    }

}
