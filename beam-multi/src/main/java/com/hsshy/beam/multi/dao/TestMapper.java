package com.hsshy.beam.multi.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hsshy.beam.multi.entity.Test;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fengshuonan
 * @since 2018-07-10
 */
public interface TestMapper extends BaseMapper<Test> {

}
