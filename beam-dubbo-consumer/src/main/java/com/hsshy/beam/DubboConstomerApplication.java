package com.hsshy.beam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DubboConstomerApplication {

    public static void main(String[] args) {
        SpringApplication.run(DubboConstomerApplication.class, args);
    }

}
