package com.hsshy.beam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BeamRabbitmqApplication {

    public static void main(String[] args) {
        SpringApplication.run(BeamRabbitmqApplication.class, args);
    }

}
